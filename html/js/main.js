var confidence_levels=Array();
var language = "English";
var last_speech_status = "Idle";

var subs_sp_detected = null
var subs_wrd_recognized = null
var subs_cur_sentence = null
var subs_sw_language = null

var SHOW_SUBTITLES_VARIABLE = "show_subtitles";

function onFrontTactilTouched(value)
{
    console.log("FrontTactilTouched event", value);
    if (value) document.getElementById("touch").innerHTML = "touched";
    else document.getElementById("touch").innerHTML = "Not touched";
}

function onSpeechDetected(value)
{
    console.log("SpeechDetected event", value);
    if (value) {
	$("#speech").html("💬");
    }
    else {
	$("#speech").html("");
    }
}

function onWordRecognized(value)
{
    //console.log("WordRecognized event", value);
    console.log("WordRecognizedAndGrammar event", value);
    document.getElementById("word").innerHTML = value;
}

function setLanguage(grammar)
{
    var new_language = '';
    if (grammar.search("Czech") >= 0) new_language = "Czech";
    else if (grammar == "") new_language = language;
    else new_language = "English";
    if (new_language != language) {
	console.log("Changing language to", new_language);
	language = new_language;
	onSpeechStatus(last_speech_status);
    }
}

function isConfident(word, confidence, grammar)
{
    grammar = grammar.slice(0,3);
    var threshold = confidence_levels[grammar]["Default"];
    if (confidence >= threshold) {
	console.log("confidence",confidence,"with grammar", grammar,">=",threshold);
	return (true);
    }
    console.log("confidence",confidence,"with grammar", grammar,"<",threshold);
    return (false);
}

function onLastInput(value)
{
    //console.log("LastInput event", value);
    console.log("WordRecognizedAndGrammar event", value);
    var word = value[0];
    var confidence = value[1];
    var grammar = value[2];
    setLanguage(grammar);
    if (word) {
	$("#word").html(word);
	$("#word").removeClass("inactive");
	if (isConfident(word,confidence,grammar)) {
	    $("#word").removeClass("activenotconfident");
	    $("#word").addClass("activeconfident");
	}
	else {
	    $("#word").removeClass("activeconfident");
	    $("#word").addClass("activenotconfident");
	}
    }
    else {
	$("#word").html("-");
	$("#word").removeClass("activeconfident");
	$("#word").removeClass("activenotconfident");
	$("#word").addClass("inactive");
    }
}

function onAnswered(value)
{
    console.log("ALTextToSpeech/CurrentSentence event", value);
    if (value) {
	$("#answer").html(value);
	$("#answer").removeClass("inactive");
	$("#answer").addClass("active");
    }
    //else {
    //    $("#answer").html("-");
    //    $("#answer").removeClass("active");
    //    $("#answer").addClass("inactive");
    //}
}

function onLanguageTTS(value)
{
    console.log("ALTextToSpeech/languageTTS event", value);
    if (value) setLanguage(value.charAt(0).toUpperCase() + value.slice(1));
}

function onSpeechStatus(value)
{
    //console.log("ALSpeechRecognition/Status event", value,typeof(value));
    console.log("Dialog/IsStarted event", value, typeof(value));
    if (value == "1" || value == true || value == "ListenOn") {
	last_speech_status = value;
	$("#status").addClass("activestatus");
	if (language == "Czech") $("#status").html("Mluvte prosím!");
	else $("#status").html("Speak to me!");
	subs_sp_detected    = RobotUtils.subscribeToALMemoryEvent("SpeechDetected", onSpeechDetected);
	subs_wrd_recognized = RobotUtils.subscribeToALMemoryEvent("WordRecognizedAndGrammar", onLastInput);
	subs_cur_sentence   = RobotUtils.subscribeToALMemoryEvent("ALTextToSpeech/CurrentSentence", onAnswered);
	subs_sw_language    = RobotUtils.subscribeToALMemoryEvent("Dialog/SwitchLanguage", onLanguageTTS);
    }
    else if (value == "0" || value == false || value == "ListenOff" || value == "Idle" || value == "Stop") {
	last_speech_status = value;
	$("#status").addClass("inactivestatus");
	if (language == "Czech") $("#status").html("čekejte prosím");
	else $("#status").html("wait please");
	if (subs_sp_detected) { subs_sp_detected.unsubscribe(); subs_sp_detected = null; }
	if (subs_wrd_recognized) { subs_wrd_recognized.unsubscribe(); subs_wrd_recognized = null; }
	if (subs_cur_sentence) { subs_cur_sentence.unsubscribe(); subs_cur_sentence = null; }
	if (subs_sw_language) { subs_sw_language.unsubscribe(); subs_sw_language = null; }
    }
    else if (value == "SpeechDetected" || value == "EndOfProcess") {
	// no change
    }
}

function update_variable(stateJson) {
    var num = stateJson;
    console.log("update variable", SHOW_SUBTITLES_VARIABLE, stateJson);
    if (num != undefined && num == '1') {
	onSpeechStatus(num);
    }
}


var application = function(){
    RobotUtils.onService(function (ALDialogSubtitles) {
        $("#noservice").hide();
	$("#exit").show();
        $("#exit").click(function() {
            ALDialogSubtitles.show_tablet(0);
        })
    }, function() {
        console.log("Failed to get the service.")
        $("#noservice").show();
    });
    RobotUtils.onService(function (ALDialog) {
        ALDialog.getAllConfidenceThresholds().then(function(levels) {
            confidence_levels = levels;
	    console.log("ALDialog confidence thresholds:", confidence_levels);
	    console.log("BNF confidence threshold:", confidence_levels["BNF"]["Default"]);
        });
    }, function() {
        console.log("Failed to get ALDialog confidence thresholds.")
    });
    RobotUtils.onService(function (ALMemory) {
	ALMemory.getData(SHOW_SUBTITLES_VARIABLE).then(update_variable);
    });
    //RobotUtils.onService(function (ALSpeechRecognition) {
    //    ALSpeechRecognition.subscribe('dialog_subtitles').then(function() {
    //        console.log("ALSpeechRecognition subscribed");
    //    });
    //}, function() {
    //    console.log("Failed to subscribe to ALSpeechRecognition")
    //});
    ////RobotUtils.subscribeToALMemoryEvent("FrontTactilTouched", onFrontTactilTouched);
    //RobotUtils.subscribeToALMemoryEvent("SpeechDetected", onSpeechDetected);
    ////RobotUtils.subscribeToALMemoryEvent("ALSpeechRecognition/Status", onSpeechStatus);
    RobotUtils.subscribeToALMemoryEvent("Dialog/IsStarted", onSpeechStatus);
    ////RobotUtils.subscribeToALMemoryEvent("ALSpeechRecognition/ActiveListening", onSpeechStatus);
    ////RobotUtils.subscribeToALMemoryEvent("WordRecognized", onWordRecognized);
    ////RobotUtils.subscribeToALMemoryEvent("WordRecognizedAndGrammar", onWordRecognized);
    ////RobotUtils.subscribeToALMemoryEvent("Dialog/LastInput", onLastInput);
    ////RobotUtils.subscribeToALMemoryEvent("Dialog/Answered", onAnswered);
    //RobotUtils.subscribeToALMemoryEvent("WordRecognizedAndGrammar", onLastInput);
    //RobotUtils.subscribeToALMemoryEvent("ALTextToSpeech/CurrentSentence", onAnswered);
    //RobotUtils.subscribeToALMemoryEvent("Dialog/SwitchLanguage", onLanguageTTS);
}

/*
var KEY_TABLETSETATE = "dialog_subtitles/TabletState";

function updateTabletState(stateJson) {
    var state = JSON.parse(stateJson);
    if (state.title) {
        $("#biglabel").show();
        $("#biglabel").html(state.title);
    } else {
        $("#biglabel").hide();
    }
}

function onFrontTactilTouched(value)
{
    console.log("FrontTactilTouched event", value);
    if (value) document.getElementById("touch").innerHTML = "touched";
    else document.getElementById("touch").innerHTML = "Not touched";
}

function onSpeechDetected(value)
{
    console.log("SpeechDetected event", value);
    if (value) {
	$("#speech").html("Speech detected");
	$("#speech").removeClass("inactive");
	$("#speech").addClass("active");
    }
    else {
	$("#speech").html("Speech not detected");
	$("#speech").removeClass("active");
	$("#speech").addClass("inactive");
    }
}

function onWordRecognized(value)
{
    //console.log("WordRecognized event", value);
    console.log("WordRecognizedAndGrammar event", value);
    document.getElementById("word").innerHTML = value;
}

function onSpeechStatus(value)
{
    console.log("ALSpeechRecognition/Status event", value);
    document.getElementById("status").innerHTML = value;
}

var application = function(){
    RobotUtils.onService(function (ALDialogSubtitles) {
        $("#noservice").hide();
        ALDialogSubtitles.get().then(function(level) {
            // Find the button with the right level:
            $(".levelbutton").each(function() {
                var button = $(this);
                if (button.data("level") == level) {
                    button.addClass("highlighted");
                    button.addClass("clicked");
                }
            });
            // ... and show all buttons:
            $("#buttons").show();
        });
        $(".levelbutton").click(function() {
            // grey out the button, until we hear back that the click worked.
            var button = $(this);
            var level = button.data("level");
            $(".levelbutton").removeClass("highlighted");
            $(".levelbutton").removeClass("clicked");
            button.addClass("clicked");
            ALDialogSubtitles.set(level).then(function(){
                button.addClass("highlighted");
            });
        })
    }, function() {
        console.log("Failed to get the service.")
        $("#noservice").show();
    });
    RobotUtils.onService(function(ALTextToSpeech) {
        // Bind button callbacks
        $(".bleeper").click(function() {
            ALTextToSpeech.say($(this).html());
        });
    });
    RobotUtils.subscribeToALMemoryEvent("FrontTactilTouched", onFrontTactilTouched);
    RobotUtils.subscribeToALMemoryEvent("SpeechDetected", onSpeechDetected);
    RobotUtils.subscribeToALMemoryEvent("ALSpeechRecognition/Status", onSpeechStatus);
    //RobotUtils.subscribeToALMemoryEvent("WordRecognized", onWordRecognized);
    RobotUtils.subscribeToALMemoryEvent("WordRecognizedAndGrammar", onWordRecognized);
    RobotUtils.onService(function (ALMemory) {
	ALMemory.getData(KEY_TABLETSETATE).then(updateTabletState);
	//updateTabletState('{"title": "TEST"}');
    });
    RobotUtils.subscribeToALMemoryEvent(KEY_TABLETSETATE, updateTabletState);
 */
