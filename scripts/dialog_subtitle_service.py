#!/usr/bin/python
# -*- coding: utf-8 -*-
#
"""
NAOqi service for subtitles
"""

__version__ = "0.0.1"

__copyright__ = "Copyright (C) 2019, NLP Centre, FI MU"
__author__ = 'Ales Horak'
__email__ = 'hales@fi.muni.cz'


import qi

import json
import time

import stk.runner
import stk.events
import stk.services
import stk.logging

class ALDialogSubtitles(object):
    APP_ID = "com.aldebaran.ALDialogSubtitles"
    def __init__(self, qiapp):
        # generic activity boilerplate
        self.qiapp = qiapp
        self.events = stk.events.EventHelper(qiapp.session)
        self.s = stk.services.ServiceCache(qiapp.session)
        self.logger = stk.logging.get_logger(qiapp.session, self.APP_ID)
        # Internal variables
        self.touch_watched = False

    @qi.bind(returnType=qi.Bool, paramsType=[qi.Bool])
    def show_tablet(self, setOn):
        if not self.s._ALAutonomousTablet:
            self.s.reset_service("_ALAutonomousTablet")
        if str(setOn) == 'True':
            if self.s.ALMemory:
                self.s.ALMemory.insertData("show_subtitles","1")
            else:
                self.logger.warning("ALMemory service not found")
            if self.s._ALAutonomousTablet:
                self.s._ALAutonomousTablet.setEnabled(0)
            else:
                self.logger.warning("_ALAutonomousTablet service not found")
            if self.s.ALTabletService:
                self.s.ALTabletService.showWebview("http://198.18.0.1/apps/dialog_subtitles")
            else:
                self.logger.warning("ALTabletService not found")
            self.set_touch_watched(False)
            return (True)
        else:
            if self.s.ALTabletService:
                self.s.ALTabletService.hideWebview()
            else:
                self.logger.warning("ALTabletService not found")
            if self.s._ALAutonomousTablet:
                self.s._ALAutonomousTablet.setEnabled(1)
            else:
                self.logger.warning("_ALAutonomousTablet service not found")
            self.set_touch_watched(True)
            return (False)

    @qi.bind(returnType=qi.Bool, paramsType=[qi.Bool])
    def set_touch_watched(self, setOn):
        if str(setOn) == 'True':
            if not self.touch_watched and self.s.ALTabletService:
                self.events.connect("ALTabletService.onTouchDown", self.on_touched)
                self.logger.info("ALDialogSubtitles added on_touch callback")
                self.touch_watched = True
            else:
                self.logger.warning("ALTabletService service not found - cannot set on_touch callback")
        else:
            if self.touch_watched:
                self.events.disconnect("ALTabletService.onTouchDown")
                self.logger.info("ALDialogSubtitles removed on_touch callback")
                self.touch_watched = False
        return (self.touch_watched)

    @qi.bind(returnType=qi.Bool, paramsType=[])
    def is_touch_watched(self):
        return (self.touch_watched)

    @qi.nobind
    def on_touched(self, *args):
        "Callback for tablet touched."
        if args:
            self.logger.info("ALDialogSubtitles tablet touched: " + str(args))
            self.show_tablet(True)

    @qi.nobind
    def on_start(self):
        self.show_tablet(True)

    @qi.bind(returnType=qi.Void, paramsType=[])
    def stop(self):
        "Stop the service."
        self.logger.info("ALDialogSubtitles stopped by user request.")
        self.qiapp.stop()

    @qi.nobind
    def on_stop(self):
        "Cleanup (add yours if needed)"
        self.logger.info("ALDialogSubtitles finished.")
        self.events.disconnect("ALTabletService.onTouchDown")
        self.events.clear()

####################
# Setup and Run
####################

if __name__ == "__main__":
    stk.runner.run_service(ALDialogSubtitles)

