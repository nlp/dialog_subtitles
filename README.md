# Pepper Subtitles

Developed at [NLP Centre](https://nlp.fi.muni.cz/en), [FI MU](https://www.fi.muni.cz/index.html.en) for [Karel Pepper](https://nlp.fi.muni.cz/trac/pepper)

The application may by started by the Czech ("titulky") or English ("subtitles") command.

After starting, the tablet displays what the robot understands and what he answers back.  
 ![Pepper subtitles](pepper_subtitles.png "Pepper subtitles"){width=400px : .shadow}</center>

The subtitles may be also displayed remotely using the http://robot.IP.address/apps/dialog_subtitles/ URL.

## Installation

* [make and install](https://nlp.fi.muni.cz/trac/pepper/wiki/InstallPkg) the package as usual for the Pepper robot

see https://nlp.fi.muni.cz/trac/pepper/wiki/NlpPepperShows for a video of
the subtitles in action
