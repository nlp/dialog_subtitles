<?xml version="1.0" encoding="UTF-8" ?>
<Package name="dialog_subtitles" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions/>
    <Dialogs>
        <Dialog name="dlg_subtitles" src="dlg_subtitles/dlg_subtitles.dlg" />
    </Dialogs>
    <Resources>
        <File name="style" src="html/css/style.css" />
        <File name="index" src="html/index.html" />
        <File name="icon" src="icon.png" />
        <File name="jquery-1.11.0.min" src="html/js/jquery-1.11.0.min.js" />
        <File name="main" src="html/js/main.js" />
        <File name="robotutils" src="html/js/robotutils.js" />
        <File name="dialog_subtitle_service" src="scripts/dialog_subtitle_service.py" />
        <File name="__init__" src="scripts/stk/__init__.py" />
        <File name="events" src="scripts/stk/events.py" />
        <File name="logging" src="scripts/stk/logging.py" />
        <File name="runner" src="scripts/stk/runner.py" />
        <File name="services" src="scripts/stk/services.py" />
    </Resources>
    <Topics>
        <Topic name="dlg_subtitles_czc" src="dlg_subtitles/dlg_subtitles_czc.top" topicName="dlg_subtitles" language="cs_CZ" />
        <Topic name="dlg_subtitles_enu" src="dlg_subtitles/dlg_subtitles_enu.top" topicName="dlg_subtitles" language="en_US" />
    </Topics>
    <IgnoredPaths>
        <Path src=".metadata" />
    </IgnoredPaths>
</Package>
